package cn.pxj.pethome.base.utils;

import lombok.Data;

import java.util.List;

@Data
public class ResultBean<T> {
    //总条数
    private Long total;

    private List<T> pageList;
}
