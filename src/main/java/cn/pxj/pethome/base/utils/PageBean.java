package cn.pxj.pethome.base.utils;

import lombok.Data;

@Data
public class PageBean {
    private Integer currentPage = 1;
    private Integer pageSize = 5;

    public Integer getPage(){
        return (currentPage-1)*pageSize;
    }


}
