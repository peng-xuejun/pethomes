package cn.pxj.pethome.dept.mapper;

import cn.pxj.pethome.dept.domain.Department;
import cn.pxj.pethome.dept.query.DepartmentQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IDepartmentMapper {
    /**
     * 查询所有数据
     */
    List<Department> findall(DepartmentQuery query);

    /**
     * 查询所有条数
     * @param query
     * @return
     */
    Long findcountall(DepartmentQuery query);
    /**
     * 查询一条数据
     */
    Department findone(Long id);
    /**
     * 删除一条数据
     */
    void delect(Long id);
    /**
     * 修改数据
     */
    void update(Department department);
    /**
     * 新增数据
     */
    void addDeptment(Department department);

}
