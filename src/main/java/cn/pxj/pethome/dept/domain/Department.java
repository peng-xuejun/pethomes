package cn.pxj.pethome.dept.domain;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Department {
    private Long id;
    private String sn;
    private String name;
    private String dirPath;
    private Employee manager;
    private Integer state;
    private Long manager_id;
    private Long parent_id;
    private Department parent;



}
