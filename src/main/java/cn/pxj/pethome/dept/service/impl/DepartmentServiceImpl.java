package cn.pxj.pethome.dept.service.impl;

import cn.pxj.pethome.base.utils.ResultBean;
import cn.pxj.pethome.dept.domain.Department;
import cn.pxj.pethome.dept.mapper.IDepartmentMapper;
import cn.pxj.pethome.dept.query.DepartmentQuery;
import cn.pxj.pethome.dept.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl implements IDepartmentService {
    @Autowired
    private IDepartmentMapper iDepartmentMapper;

    @Override
    public ResultBean<Department> queryAll(DepartmentQuery query) {

        ResultBean<Department> result = new ResultBean<>();
        result.setTotal(iDepartmentMapper.findcountall(query));
        result.setPageList(iDepartmentMapper.findall(query));
        return result;

    }

    @Override
    public Department findone(Long id) {
        return iDepartmentMapper.findone(id);
    }

    @Override
    public void addDeptment(Department department) {
        iDepartmentMapper.addDeptment(department);
    }

    @Override
    public void delect(Long id) {
        iDepartmentMapper.delect(id);
    }

    @Override
    public void update(Department department) {
        iDepartmentMapper.update(department);
    }
}
