package cn.pxj.pethome.dept.service;

import cn.pxj.pethome.base.utils.ResultBean;
import cn.pxj.pethome.dept.domain.Department;
import cn.pxj.pethome.dept.query.DepartmentQuery;

public interface IDepartmentService {

    /**
     * 高级查询和分页查询
     * @param query
     * @return
     */
    ResultBean<Department> queryAll(DepartmentQuery query);

    /**
     * 查询一条数据
     */
    Department findone(Long id);
    /**
     * 添加部门
     */
    void addDeptment(Department department);

    /**
     * 删除数据
     * @param id
     */
    void delect(Long id);
    /**
     * 修改数据
     */
    void update(Department department);

}
