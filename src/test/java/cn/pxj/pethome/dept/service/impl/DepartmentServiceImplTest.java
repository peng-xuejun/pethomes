package cn.pxj.pethome.dept.service.impl;


import cn.pxj.pethome.base.utils.ResultBean;
import cn.pxj.pethome.dept.domain.Department;
import cn.pxj.pethome.dept.query.DepartmentQuery;
import cn.pxj.pethome.dept.service.IDepartmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DepartmentServiceImplTest {
    @Autowired
    private IDepartmentService iDepartmentService;

    @Test
    public void queryAll() {
        DepartmentQuery departmentQuery = new DepartmentQuery();
        ResultBean<Department> departmentResultBean = iDepartmentService.queryAll(departmentQuery);
        departmentResultBean.getPageList().forEach(System.out::println);
    }

    @Test
    public void findone() {
        Department findone = iDepartmentService.findone(1L);
        System.out.println(findone);

    }

    @Test
    public void addDeptment() {

        Department department = new Department();
        department.setId(66L);
        department.setName("部门4");
        iDepartmentService.update(department);

    }

    @Test
    public void delect() {
        iDepartmentService.delect(2L);
    }

    @Test
    public void update() {


    }
}